package database

import (
	"errors"
	"fmt"

	as "github.com/aerospike/aerospike-client-go"
)

//AccountSet is the Set name of Account data
const AccountSet = "Account"

const (
	binID       string = "ID"
	binPassHash string = "PassHash"
	binAccType  string = "AccType"
	binUID      string = "UID"
)

//ErrAccountNotFound occure when Account not found in DB
var ErrAccountNotFound = errors.New("Account Not Found")

//SaveNewAccount save a new account to db and its saves
func SaveNewAccount(acc Account, client *as.Client) (err error) {

	err = createNewUserSave(acc.UID, client)
	if err != nil {
		return err
	}
	fmt.Println("Create UserSave")
	key, err := as.NewKey(DBNamespace, AccountSet, acc.ID)
	if err != nil {
		return err
	}
	bin := as.NewBin(binID, acc.ID)
	bin2 := as.NewBin(binPassHash, acc.PassHash)
	bin3 := as.NewBin(binUID, acc.UID)
	bin4 := as.NewBin(binAccType, acc.AccType)
	err = client.PutBins(nil, key, bin, bin2, bin3, bin4)
	return err
}

//GetAccount return user Account using ID
func GetAccount(ID string, client *as.Client) (acc Account, err error) {
	acc = Account{}
	key, err := as.NewKey(DBNamespace, AccountSet, ID)
	if err != nil {
		return acc, err
	}
	rec, err := client.Get(nil, key, binID, binPassHash, binUID, binAccType)
	if err != nil {
		return acc, err
	}
	if rec == nil {
		return acc, ErrAccountNotFound
	}
	var ok bool
	acc.ID, ok = rec.Bins[binID].(string)
	if ok == false {
		acc.ID = ""
	}
	acc.PassHash, ok = rec.Bins[binPassHash].(string)
	if ok == false {
		acc.PassHash = ""
	}
	acc.UID, ok = rec.Bins[binUID].(int)
	if ok == false {
		acc.UID = 0
	}
	acc.AccType, ok = rec.Bins[binAccType].(int)
	if ok == false {
		acc.AccType = 0
	}
	return acc, nil
}

//ChangePass change Account ID's Pass
func ChangePass(ID string, PassHash string, client *as.Client) (err error) {
	ok := ValidAccount(ID, client)
	if ok == false {
		return ErrAccountNotFound
	}
	key, err := as.NewKey(DBNamespace, AccountSet, ID)
	if err != nil {
		return err
	}
	bin := as.NewBin(binPassHash, PassHash)
	err = client.PutBins(nil, key, bin)
	return err
}

//ValidAccount check ID is valid
func ValidAccount(ID string, client *as.Client) bool {
	key, err := as.NewKey(DBNamespace, AccountSet, ID)
	if err != nil {
		fmt.Println(err)
		return false
	}
	ok, err := client.Exists(nil, key)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return ok
}

//GetNewUID add user number by 1 and return the new number, used by account creation
func GetNewUID(client *as.Client) (i int, err error) {
	key, err := as.NewKey(DBNamespace, "UserCount", "NumOfUser")
	if err != nil {
		return 0, err
	}
	i, err = GetUserNum(client)
	if err != nil {
		return 0, err
	}
	i++
	fmt.Println(i)
	bin := as.NewBin("Count", i)
	err = client.PutBins(nil, key, bin)
	return i, err
}

//GetNewTID add Team number by 1 and return the new number, used by Team creation
func GetNewTID(client *as.Client) (i int, err error) {
	key, err := as.NewKey(DBNamespace, "UserCount", "NumOfTeam")
	if err != nil {
		return 0, err
	}
	i, err = GetUserNum(client)
	if err != nil {
		return 0, err
	}
	i++
	fmt.Println(i)
	bin := as.NewBin("Count", i)
	err = client.PutBins(nil, key, bin)
	return i, err
}
