package database

import (
	"errors"
	"fmt"
	"sync"
	"time"

	as "github.com/aerospike/aerospike-client-go"
)

//TeamSaveSet is the Set of team save data
const TeamSaveSet = "TeamSaves"

//ErrTeamNotFound occure when team not found in DB
var ErrTeamNotFound = errors.New("Team Not Found")

//PunishTimeDuration is the time(minute) gap between answering wrong question
const PunishTimeDuration = 10

//CreateNewTeam create a new team in db and join UID in to the team and return TID
func CreateNewTeam(UID int, client *as.Client) (TID int, err error) {
	//Check UID Valid
	ok := ValidUID(UID, client)
	if ok == false {
		return 0, errors.New("UID not exist")
	}
	//Get a new TID
	TID, err = GetNewUID(client)
	if err != nil {
		return 0, err
	}
	//Create the record
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return 0, err
	}
	//Create team member team
	bin1 := as.NewBin("TID", TID)
	bin2 := as.NewBin("LeaderUID", UID)
	err = client.PutBins(nil, key, bin1, bin2)
	if err != nil {
		return 0, err
	}
	//Join into team
	err = JoinTeam(UID, TID, client)

	return TID, err
}

//SetTeamName set TID team Name
func SetTeamName(TID int, Name string, client *as.Client) (err error) {
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return err
	}
	rec, err := client.Get(nil, key, "TeamName")
	if err != nil {
		return err
	}
	//No record found, there is no TID team
	if rec == nil {
		return ErrTeamNotFound
	}
	bin := as.NewBin("TeamName", Name)
	err = client.PutBins(nil, key, bin)
	return err
}

//GetTeamData get the progress and LogID of team
func GetTeamData(TID int, client *as.Client) (save TeamSave, err error) {
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	rec, err := client.Get(nil, key, "LogID", "LogTeller", "Progress")
	if err != nil {
		return TeamSave{}, err
	}
	if rec == nil {
		return TeamSave{}, ErrTeamNotFound
	}
	Logs, ok := rec.Bins["LogID"].([]interface{})
	// if there were no logs , create a new log
	if ok == false {
		Logs = []interface{}{}
	}
	Progress, ok := rec.Bins["Progress"].([]interface{})
	// if there were no logs , create a new log
	if ok == false {
		Progress = []interface{}{}
	}
	LogTeller, ok := rec.Bins["LogTeller"].([]interface{})
	// if there were no logs , create a new log
	if ok == false {
		LogTeller = []interface{}{}
	}

	return TeamSave{TID: TID, Progress: Progress, Logs: Logs, LogTeller: LogTeller}, nil
}

var logMapMutex = &sync.Mutex{}

//teamLogMutexes this map store different mutex for different TID
// to prevent multiple write of TeamLog in the same time
var teamLogMutexes = make(map[int]*sync.Mutex)

//AppendTeamLog add a new log into log id array
func AppendTeamLog(TID int, UID int, LogID int, client *as.Client) (err error) {
	// need to make sure there are no 2 team member are adding log
	// check if have mutex
	logMapMutex.Lock()
	mutex, ok := teamLogMutexes[TID]
	//if there is no mutex for this TID  create one
	if ok == false {
		mutex = &sync.Mutex{}
		teamLogMutexes[TID] = mutex
	}
	logMapMutex.Unlock()

	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return err
	}
	//lock read and write section to prevent overwrite
	mutex.Lock()
	defer mutex.Unlock()
	//read Log
	rec, err := client.Get(nil, key, "LogID", "LogTeller")
	if err != nil {
		return err
	}
	// if no record , means no TID team
	if rec == nil {
		return ErrTeamNotFound
	}
	Logs, ok := rec.Bins["LogID"].([]interface{})
	// if there were no logs , create a new log
	if ok == false {
		Logs = []interface{}{}
	}
	LogTellers, ok := rec.Bins["LogTeller"].([]interface{})
	// if there were no logs , create a new log
	if ok == false {
		LogTellers = []interface{}{}
	}
	// append new LogID to old Logs
	Logs = append(Logs, LogID)
	LogTellers = append(LogTellers, UID)
	// write back to database
	bin := as.NewBin("LogID", Logs)
	bin2 := as.NewBin("LogTeller", LogTellers)
	err = client.PutBins(nil, key, bin, bin2)
	return err
}

//ClearTeamLog clear the team log
func ClearTeamLog(TID int, client *as.Client) (err error) {
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return err
	}
	bin := as.NewBin("LogID", []int{})
	bin2 := as.NewBin("LogTeller", []int{})
	err = client.PutBins(nil, key, bin, bin2)
	return err
}

var progressesMapMutex = &sync.Mutex{}

//teamProgressMutexes this map store different mutex for different TID
// to prevent multiple write of TeamProgress in the same time
var teamProgressMutexes = make(map[int]*sync.Mutex)

//AppendTeamProgress add a new progress into progress id array
func AppendTeamProgress(TID int, ProgressID int, client *as.Client, checkPunish bool) (err error) {

	if checkPunish {
		if IsInPunishTime(TID) {
			return errors.New("Team is in Punish Time")
		}
	}

	// need to make sure there are no 2 team member are adding progress
	// check if have mutex
	progressesMapMutex.Lock()
	mutex, ok := teamProgressMutexes[TID]
	//if there is no mutex for this TID  create one
	if ok == false {
		mutex = &sync.Mutex{}
		teamProgressMutexes[TID] = mutex
	}
	progressesMapMutex.Unlock()

	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return err
	}
	//lock read and write section to prevent overwrite
	mutex.Lock()
	defer mutex.Unlock()
	fmt.Println("StartAppend")
	//read Log
	rec, err := client.Get(nil, key, "Progress")
	if err != nil {
		return err
	}
	// if no record , means no TID team
	if rec == nil {
		return ErrTeamNotFound
	}
	Progress, ok := rec.Bins["Progress"].([]interface{})
	// if there were no logs , create a new log
	if ok == false {
		Progress = []interface{}{}
	}
	// append new LogID to old Logs
	Progress = append(Progress, ProgressID)
	// write back to database
	bin := as.NewBin("Progress", Progress)
	err = client.PutBins(nil, key, bin)
	fmt.Print("Done Append : ")
	fmt.Println(Progress)
	return err
}

//ClearTeamProgress clear the team log
func ClearTeamProgress(TID int, client *as.Client) (err error) {
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return err
	}
	bin := as.NewBin("Progress", []int{})
	err = client.PutBins(nil, key, bin)
	return err
}

//ValidTID check UID is valid
func ValidTID(TID int, client *as.Client) bool {
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return false
	}
	ok, err := client.Exists(nil, key)
	if err != nil {
		return false
	}
	return ok
}

var punishTimeMapMutex = &sync.Mutex{}

//punishTimeMutexes this map store different mutex for different TID
// to prevent multiple write of Team punish time in the same time
var punishTimeMutexes = make(map[int]*sync.Mutex)

var teamPunishTime = make(map[int]time.Time)

//ResetPunishTime reset the punish time of team TID to now
func ResetPunishTime(TID int) {
	punishTimeMapMutex.Lock()
	mutex, ok := punishTimeMutexes[TID]
	//if there is no mutex for this TID  create one
	if ok == false {
		mutex = &sync.Mutex{}
		punishTimeMutexes[TID] = mutex
	}
	punishTimeMapMutex.Unlock()

	//lock read and write section to prevent overwrite
	mutex.Lock()
	defer mutex.Unlock()
	teamPunishTime[TID] = time.Now().Add(time.Minute * PunishTimeDuration)
}

//IsInPunishTime Check if team can add progress
func IsInPunishTime(TID int) (ok bool) {
	punishTimeMapMutex.Lock()
	mutex, ok := punishTimeMutexes[TID]
	//if there is no mutex for this TID  create one
	if ok == false {
		mutex = &sync.Mutex{}
		punishTimeMutexes[TID] = mutex
	}
	punishTimeMapMutex.Unlock()

	mutex.Lock()
	defer mutex.Unlock()

	ori, ok := teamPunishTime[TID]
	if ok == false {
		return false
	}
	t := time.Now()
	elap := t.Sub(ori)
	if elap > 0 {
		return false
	}
	return true
}

//GetPunishTime of Team TID
func GetPunishTime(TID int) time.Time {
	punishTimeMapMutex.Lock()
	mutex, ok := punishTimeMutexes[TID]
	//if there is no mutex for this TID  create one
	if ok == false {
		mutex = &sync.Mutex{}
		punishTimeMutexes[TID] = mutex
	}
	punishTimeMapMutex.Unlock()

	mutex.Lock()
	defer mutex.Unlock()

	ori, ok := teamPunishTime[TID]
	if ok == false {
		return time.Time{}
	}
	return ori
}

var teamMemberMapMutex = &sync.Mutex{}

//punishTimeMutexes this map store different mutex for different TID
// to prevent multiple write of Team punish time in the same time
var teamMemberMutexes = make(map[int]*sync.Mutex)

func getTeamMemberMutex(TID int) (mutex *sync.Mutex) {
	teamMemberMapMutex.Lock()
	mutex, ok := teamMemberMutexes[TID]
	//if there is no mutex for this TID  create one
	if ok == false {
		mutex = &sync.Mutex{}
		teamMemberMutexes[TID] = mutex
	}
	teamMemberMapMutex.Unlock()
	return mutex
}

//SetTeamMemberName set the nickname of UID in Team TID to Name
func SetTeamMemberName(TID int, UID int, Name string, client *as.Client) (err error) {

	mutex := getTeamMemberMutex(TID)

	// check TID exist
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	mutex.Lock()
	defer mutex.Unlock()

	rec, err := client.Get(nil, key, "TeamMembers")
	if err != nil {
		return err
	}
	if rec == nil {
		return ErrTeamNotFound
	}
	// check member exist
	receivedMap := rec.Bins["TeamMembers"].(map[interface{}]interface{})
	if _, ok := receivedMap[UID]; ok == false {
		return errors.New("Team member does not exist")
	}
	// change member nickname
	receivedMap[UID] = Name
	bin := as.NewBin("TeamMembers", receivedMap)
	err = client.PutBins(nil, key, bin)
	return err
}

//JoinTeam User UID Join into Team TID
func JoinTeam(UID int, TID int, client *as.Client) (err error) {
	user, err := GetUserSave(UID, client)
	if user.CurrentTeam != 0 {
		return errors.New("User already have a team")
	}
	//get mutex
	mutex := getTeamMemberMutex(TID)

	//define key
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return err
	}
	//Lock mutex and start map manipulation
	mutex.Lock()
	defer mutex.Unlock()
	rec, err := client.Get(nil, key, "TeamMembers")
	if err != nil {
		return err
	}
	//No record found, there is no TID team
	if rec == nil {
		return ErrTeamNotFound
	}

	MemberMap, ok := rec.Bins["TeamMembers"].(map[interface{}]interface{})
	//check Member array is valid
	if ok == false {
		MemberMap = make(map[interface{}]interface{})
	}
	MemberMap[UID] = user.Name //FIX  change to member name
	bin := as.NewBin("TeamMembers", MemberMap)
	err = client.PutBins(nil, key, bin)
	if err != nil {
		return err
	}
	err = setCurrentTeam(UID, TID, client)
	return err
}

//KickMember UID from Team TID
func KickMember(UID int, TID int, client *as.Client) (err error) {
	mutex := getTeamMemberMutex(TID)

	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return err
	}
	mutex.Lock()
	defer mutex.Unlock()
	rec, err := client.Get(nil, key, "TeamMembers")
	if err != nil {
		return err
	}
	//No record found, there is no TID team
	if rec == nil {
		return ErrTeamNotFound
	}
	MemberMap, ok := rec.Bins["TeamMembers"].(map[interface{}]interface{})
	//check Member array is valid
	if ok == false {
		MemberMap = make(map[interface{}]interface{})
	}
	delete(MemberMap, UID)
	bin := as.NewBin("TeamMembers", MemberMap)
	err = client.PutBins(nil, key, bin)
	if err != nil {
		return err
	}
	err = LeaveTeam(UID, client)
	return err
}

//GetTeamInfo from Database
func GetTeamInfo(TID int, client *as.Client) (info map[string]interface{}, err error) {
	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return nil, err
	}
	rec, err := client.Get(nil, key, "TeamMembers", "TeamName", "Job")
	if err != nil {
		return nil, err
	}
	//No record found, there is no TID team
	if rec == nil {
		return nil, ErrTeamNotFound
	}
	info = rec.Bins
	if _, ok := info["TeamName"]; ok == false {
		info["TeamName"] = ""
	}
	// need to convert map[interface{}]interface{} to map[int]interface{}
	TM := make(map[int]interface{})
	OC := make(map[int]interface{})
	// check have teammember
	if _, ok := info["TeamMembers"]; ok {
		//Create key for batch read ori name
		keys := make([]*as.Key, len(info["TeamMembers"].(map[interface{}]interface{})))

		//create a temp map
		var job map[interface{}]interface{}
		//check have job
		if _, ok := info["Job"]; ok {
			job = info["Job"].(map[interface{}]interface{})
		} else {
			// dont have job, initialize
			job = make(map[interface{}]interface{})
		}
		i := 0 // use for fill key
		//go thourgh all member
		for k, v := range info["TeamMembers"].(map[interface{}]interface{}) {
			//check key
			index, ok := k.(int)

			if ok {
				//fill key
				keys[i], _ = as.NewKey(DBNamespace, UserSaveSet, index)
				i++
				//clone
				TM[index] = v
				//if have job give job,
				if e, ok := job[index]; ok {
					fmt.Printf("set to %v", e)
					OC[index] = e
				} else {
					fmt.Println("not ok, set to 0")
					//dont have job give 0
					OC[index] = 0
				}
			}

		}
		records, err := client.BatchGet(nil, keys, "Name", "UID")
		if err != nil {
			return nil, err
		}
		oriName := make(map[int]interface{})
		for _, element := range records {
			if element != nil {
				uid, ok := element.Bins["UID"].(int)
				if ok == false {
					continue
				}
				oriName[uid] = element.Bins["Name"]
			}
		}
		info["OriName"] = oriName
	}
	info["TeamMembers"] = TM
	info["Job"] = OC

	return info, nil
}

var teamJobMapMutex = &sync.Mutex{}

//punishTimeMutexes this map store different mutex for different TID
// to prevent multiple write of Team punish time in the same time
var teamJobMutexes = make(map[int]*sync.Mutex)

//SetJob set UID jobs in team TID
func SetJob(TID int, UID int, Job int, client *as.Client) (err error) {
	//check TID
	teamJobMapMutex.Lock()
	mutex, ok := teamJobMutexes[TID]
	//if there is no mutex for this TID  create one
	if ok == false {
		mutex = &sync.Mutex{}
		teamJobMutexes[TID] = mutex
	}
	teamJobMapMutex.Unlock()

	key, err := as.NewKey(DBNamespace, TeamSaveSet, TID)
	if err != nil {
		return err
	}
	mutex.Lock()
	defer mutex.Unlock()
	rec, err := client.Get(nil, key, "Job")
	if err != nil {
		return err
	}
	//No record found, there is no TID team
	if rec == nil {
		return ErrTeamNotFound
	}
	JobMap, ok := rec.Bins["Job"].(map[interface{}]interface{})
	//check Job array is valid
	if ok == false {
		JobMap = make(map[interface{}]interface{})
	}
	//Add job
	JobMap[UID] = Job

	bin := as.NewBin("Job", JobMap)
	err = client.PutBins(nil, key, bin)

	return err
}
