package main

import (
	"fmt"

	"gitlab.com/hongping1224/SeekTainanServer/Server"
	"gitlab.com/hongping1224/SeekTainanServer/database"
)

func main() {
	/*ok := security.VerifyFBToken("10210570847093601", "EAAZAR61WcLOkBAC9uDi1CqKfqairFKf1WVfWY2jAPMNPxOGyoTYkHJgykZAJwWiZBpjRk2ch5zUU63FewFQq6UIrL0pu5kESVbM8yfWSKd3pCDrcMd3ksnMubAyzYynrv18gAsFVKO6dkyxRdKsEBPLBg0NwvZBKaCFYfZAqriSl2VltSZAJ3grpjrPX4zpzAZD")
	fmt.Printf("ok : %v \n", ok)*/
	client, err := database.NewDBClient()
	if err != nil {
		fmt.Println(err)
		return
	}
	database.InitDB()
	checkErr(err)
	serc := Server.NewHTTPServer(client)
	defer client.Close()
	serc.Start()

}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
