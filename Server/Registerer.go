package Server

import (
	"errors"
	"fmt"

	"gitlab.com/hongping1224/SeekTainanServer/database"

	as "github.com/aerospike/aerospike-client-go"
	DB "gitlab.com/hongping1224/SeekTainanServer/database"
	"gitlab.com/hongping1224/SeekTainanServer/security"
)

//ErrAccountAlreadyExist occure when Account already found in DB
var ErrAccountAlreadyExist = errors.New("Account Already Exist")

//Registerer handle all registeration
type Registerer struct {
	Waiting  map[string]DB.Account
	dbClient *as.Client
}

//Init registerer
func (register *Registerer) Init(dbclient *as.Client) {
	register.Waiting = make(map[string]DB.Account)
	register.dbClient = dbclient
}

//ViaEmail Use email and password to register
func (register *Registerer) ViaEmail(email string, pass string) (err error) {
	// check email is used
	ok := database.ValidAccount(email, register.dbClient)
	if ok == true {
		return ErrAccountAlreadyExist
	}
	// create a new account
	acc, err := register.newAccount(email, pass, DB.Email)
	if err != nil {
		return err
	}
	//Save to database
	_, err = register.saveAccountToDB(acc)
	return err
}

//ViaFB create a account using email and fb token
func (register *Registerer) ViaFB(ID string, accessToken string) (UID int, err error) {
	fmt.Println("Create Via FB")
	err = security.VerifyFBToken(ID, accessToken)
	if err != nil {
		fmt.Println("err1:", err)
		return 0, err
	}
	acc, err := register.newAccount(ID, accessToken, DB.Facebook)
	if err != nil {
		fmt.Println("err2:", err)
		return 0, err
	}
	UID, err = register.saveAccountToDB(acc)
	fmt.Println("err3:", err)
	return UID, err
}

//ViaGplus create a account using email and google token
func (register *Registerer) ViaGplus(accessToken string) (UID int, err error) {
	tokeninfo, err := security.VerifyIDToken(accessToken)
	if err != nil {
		return 0, err
	}
	acc, err := register.newAccount(tokeninfo.Email, accessToken, DB.Gplus)
	if err != nil {
		return 0, err
	}
	UID, err = register.saveAccountToDB(acc)
	return UID, err
}

//saveAccountToDB create a new account
func (register *Registerer) saveAccountToDB(user DB.Account) (UID int, err error) {
	//store into map
	UID, err = register.getNewUID()
	if err != nil {
		return 0, err
	}
	user.UID = UID
	fmt.Printf("Save Account to DB : %v\n", user)
	err = DB.SaveNewAccount(user, register.dbClient)
	if err != nil {
		fmt.Printf("Save Fail, %v", err)
		return 0, err
	}
	fmt.Println("Save Success")
	return user.UID, nil
}

func (register *Registerer) getNewUID() (UID int, err error) {
	//Get a new UID and create a new save using that UID
	UID, err = DB.GetNewUID(register.dbClient)
	if err != nil {
		return 0, err
	}
	return UID, nil
}

func (register *Registerer) newAccount(ID string, Pass string, AccType int) (acc DB.Account, err error) {
	acc = DB.Account{ID: ID, AccType: AccType}
	//generate hash using email
	hash, err := security.HashPassword(Pass)
	if err != nil {
		fmt.Printf("hashing err : %s", err)
		return acc, err
	}
	acc.PassHash = hash

	return acc, nil
}
