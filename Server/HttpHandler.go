package Server

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/hongping1224/SeekTainanServer/database"

	Util "gitlab.com/hongping1224/SeekTainanServer/Util"
)

const messageKEYString = "Message"

//test
func test(w http.ResponseWriter, req *http.Request) {
	//	msg := make(map[string]interface{})

	msg := "Hello"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		fmt.Println(err)
	}
	w.Write(pars)
}

//registerViaEmail send comfirmation email
func registerViaEmail(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	email := req.FormValue("email")
	password := req.FormValue("password")
	if email == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "email", VarType: "string", RecValue: req.FormValue("email")})
		w.Write(pars)
		return
	}
	if password == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "password", VarType: "string", RecValue: req.FormValue("password")})
		w.Write(pars)
		return
	}
	err := instance.registerer.ViaEmail(email, password)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg := "done"

	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}

	w.Write(pars)
}

func wIP(w http.ResponseWriter, req *http.Request) {

	msg := "WIP"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

//getServerTime return server current time
func getServerTime(w http.ResponseWriter, req *http.Request) {
	t := time.Now()
	msg := make(map[string]interface{})
	msg["time"] = t
	//.Format(Util.ServerTimeFormat)
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

//getUserSave return UserSaveData by UID
func getUserSave(w http.ResponseWriter, req *http.Request) {

	msg := make(map[string]interface{})
	UID, err := strconv.Atoi(req.FormValue("UID"))
	// see if UID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	// try to get user save
	save, err := instance.GetUserSave(UID)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = save
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

//saveUserSave return UserSaveData by UID
func saveUserSave(w http.ResponseWriter, req *http.Request) {

	msg := make(map[string]interface{})
	UID, err := strconv.Atoi(req.FormValue("UID"))
	PlayerSave := req.FormValue("PlayerSave")
	// see if UID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	if PlayerSave == "" {
		pars, _ := Util.PackMsg(false, "There are no Save")
		w.Write(pars)
		return
	}
	// Save user save to db
	err = instance.SaveUserSave(UID, PlayerSave)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func changeUserName(w http.ResponseWriter, req *http.Request) {

	msg := make(map[string]interface{})
	UID, err := strconv.Atoi(req.FormValue("UID"))
	Name := req.FormValue("Name")
	// see if UID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	if Name == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "Name", VarType: "string", RecValue: req.FormValue("Name")})
		w.Write(pars)
		return
	}
	// Save user save to db
	err = instance.ChangeUserName(UID, Name)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

//getUserSave return UserSaveData by UID
func getTeamSave(w http.ResponseWriter, req *http.Request) {

	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	// try to get user save
	save, err := instance.GetTeamData(TID)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = save
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func addTeamLog(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	UID, err := strconv.Atoi(req.FormValue("UID"))
	// see if UID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	LogID, err := strconv.Atoi(req.FormValue("LogID"))
	// see if LogID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "LogID", VarType: "int", RecValue: req.FormValue("LogID")})
		w.Write(pars)
		return
	}
	// try to get user save
	err = instance.AddLog(TID, UID, LogID)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func createNewTeam(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	UID, err := strconv.Atoi(req.FormValue("UID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}

	// try to get user saveUID
	TID, err := instance.CreateNewTeam(UID)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg["TID"] = TID
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func addTeamProgress(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	Progress, err := strconv.Atoi(req.FormValue("Progress"))
	// see if Progress is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "Progress", VarType: "int", RecValue: req.FormValue("Progress")})
		w.Write(pars)
		return
	}
	// try to get user save
	err = instance.AddProgressWChecks(TID, Progress)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func addFakeTeamProgress(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	Progress, err := strconv.Atoi(req.FormValue("Progress"))
	// see if Progress is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "Progress", VarType: "int", RecValue: req.FormValue("Progress")})
		w.Write(pars)
		return
	}
	// try to get user save
	err = instance.AddProgressWOChecks(TID, Progress)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func joinTeam(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	UID, err := strconv.Atoi(req.FormValue("UID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	err = instance.JoinTeam(UID, TID)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func kickMember(w http.ResponseWriter, req *http.Request) {
	fmt.Println("Kick")
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	UID, err := strconv.Atoi(req.FormValue("UID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	err = instance.KickMember(UID, TID)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func leaveTeam(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})

	UID, err := strconv.Atoi(req.FormValue("UID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	err = instance.LeaveTeam(UID)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func punishTeam(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	instance.PunishTeam(TID)
	msg[messageKEYString] = "done"
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func getPunishTime(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID Pis int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	msg["time"] = instance.GetTeamPunishTime(TID)
	//.Format(Util.ServerTimeFormat)
	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func setTeamName(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID Pis int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	Name := req.FormValue("TeamName")
	if Name == "" {
		pars, _ := Util.PackMsg(false, "There are no TeamName")
		w.Write(pars)
		return
	}

	err = instance.SetTeamName(TID, Name)
	if err != nil {
		pars, _ := Util.PackMsg(true, err)
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"

	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func setTeamNickName(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	UID, err := strconv.Atoi(req.FormValue("UID"))
	// see if UID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	Name := req.FormValue("Name")
	if Name == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "Name", VarType: "string", RecValue: req.FormValue("Name")})
		w.Write(pars)
		return
	}
	err = instance.SetTeamNickName(TID, UID, Name)
	if err != nil {
		pars, _ := Util.PackMsg(true, err)
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"

	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

//getTeamInfo return Team info by TID
func getTeamInfo(w http.ResponseWriter, req *http.Request) {

	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	// try to get user save
	save, err := instance.GetTeamInfo(TID)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg[messageKEYString] = save
	fmt.Println(save)

	pars, err := Util.PackMsg(true, save)
	if err != nil {
		fmt.Println(err)
		w.Write(pars)
		return
	}
	pars, err = Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func setJob(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	TID, err := strconv.Atoi(req.FormValue("TID"))
	// see if TID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "TID", VarType: "int", RecValue: req.FormValue("TID")})
		w.Write(pars)
		return
	}
	UID, err := strconv.Atoi(req.FormValue("UID"))
	// see if UID is int
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "UID", VarType: "int", RecValue: req.FormValue("UID")})
		w.Write(pars)
		return
	}
	Job, err := strconv.Atoi(req.FormValue("Job"))
	if err != nil {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "Job", VarType: "int", RecValue: req.FormValue("Job")})
		w.Write(pars)
		return
	}
	err = instance.SetJob(TID, UID, Job)
	if err != nil {
		pars, _ := Util.PackMsg(true, err)
		w.Write(pars)
		return
	}
	msg[messageKEYString] = "done"

	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func loginFB(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	ID := req.FormValue("ID")
	// see if ID is int
	if ID == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "ID", VarType: "string", RecValue: req.FormValue("ID")})
		w.Write(pars)
		return
	}
	pass := req.FormValue("Pass")
	// see if Pass is int
	if pass == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "Pass", VarType: "string", RecValue: req.FormValue("Pass")})
		w.Write(pars)
		return
	}

	UID, err := instance.Login(ID, pass, database.Facebook)
	if err != nil {
		fmt.Println("some error in here")
		pars, _ := Util.PackMsg(false, err)
		w.Write(pars)
		return
	}
	msg["UID"] = UID

	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func loginGplus(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	ID := req.FormValue("ID")
	// see if TID is int
	if ID == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "ID", VarType: "string", RecValue: req.FormValue("ID")})
		w.Write(pars)
		return
	}
	pass := req.FormValue("Pass")
	// see if TID is int
	if pass == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "Pass", VarType: "string", RecValue: req.FormValue("Pass")})
		w.Write(pars)
		return
	}

	UID, err := instance.Login(ID, pass, database.Gplus)
	if err != nil {
		pars, _ := Util.PackMsg(true, err)
		w.Write(pars)
		return
	}
	msg["UID"] = UID

	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}

func loginemail(w http.ResponseWriter, req *http.Request) {
	msg := make(map[string]interface{})
	ID := req.FormValue("ID")
	// see if TID is int
	if ID == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "ID", VarType: "string", RecValue: req.FormValue("ID")})
		w.Write(pars)
		return
	}
	pass := req.FormValue("Pass")
	// see if TID is int
	if pass == "" {
		pars, _ := Util.PackMsg(false, errInvalidVar{VarName: "Pass", VarType: "string", RecValue: req.FormValue("Pass")})
		w.Write(pars)
		return
	}

	UID, err := instance.Login(ID, pass, database.Email)
	if err != nil {
		pars, _ := Util.PackMsg(false, err.Error())
		w.Write(pars)
		return
	}
	msg["UID"] = UID

	pars, err := Util.PackMsg(true, msg)
	if err != nil {
		pars, err = Util.PackMsg(false, err.Error())
		if err != nil {
			pars, err = Util.PackMsg(false, err.Error())
			fmt.Println(err)
		}
	}
	w.Write(pars)
}
