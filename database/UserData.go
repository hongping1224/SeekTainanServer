package database

import (
	"errors"
	"fmt"

	as "github.com/aerospike/aerospike-client-go"
)

//UserSaveSet is the Set of user save data
const UserSaveSet = "UserSaves"

//ErrUserNotFound occure when user not found in DB
var ErrUserNotFound = errors.New("User Not Found")

func Cre(UID int, client *as.Client) (err error) {
	return createNewUserSave(UID, client)
}

//createNewUserSave create a new UID with blank save in DB
func createNewUserSave(UID int, client *as.Client) (err error) {
	//Valud UID is empty
	if ValidUID(UID, client) {
		return fmt.Errorf("UID %d already Exist", UID)
	}

	key, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	if err != nil {
		return err
	}
	bin := as.NewBin("UID", UID)
	if err != nil {
		return err
	}
	err = client.PutBins(nil, key, bin)
	return err
}

//ChangeUserName Changes the username of given UID
func ChangeUserName(UID int, name string, client *as.Client) (err error) {
	ok := ValidUID(UID, client)
	if ok == false {
		return ErrUserNotFound
	}
	key, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	if err != nil {
		return err
	}
	bin := as.NewBin("Name", name)
	if err != nil {
		return err
	}
	err = client.PutBins(nil, key, bin)
	return err
}

//SaveUserSave save user's savedata to DB
func SaveUserSave(UID int, savedata string, client *as.Client) (err error) {
	ok := ValidUID(UID, client)
	if ok == false {
		return ErrUserNotFound
	}
	key, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	if err != nil {
		return err
	}
	bin := as.NewBin("Savedata", savedata)
	if err != nil {
		return err
	}
	err = client.PutBins(nil, key, bin)
	return err
}

//GetUserSave get user save from DB
func GetUserSave(UID int, client *as.Client) (save UserSave, err error) {
	key, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	if err != nil {
		return save, err
	}
	rec, err := client.Get(nil, key, "Savedata", "Name", "CurrentTeam")
	if rec == nil {
		//fmt.Printf("UID not found %v", UID)
		return UserSave{}, ErrUserNotFound
	}
	savedata, ok := rec.Bins["Savedata"].(string)
	if ok == false {
		savedata = ""
	}
	name, ok := rec.Bins["Name"].(string)
	if ok == false {
		name = ""
	}
	TID, ok := rec.Bins["CurrentTeam"].(int)
	if ok == false {
		TID = 0
	}
	save = UserSave{UID: UID, Name: name, PlayerSave: savedata, CurrentTeam: TID}
	return save, nil
}

//ValidUID check UID is valid
func ValidUID(UID int, client *as.Client) bool {
	key, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	if err != nil {
		fmt.Println(err)
		return false
	}
	ok, err := client.Exists(nil, key)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return ok
}

//setCurrentTeam set UID current team to TID
func setCurrentTeam(UID int, TID int, client *as.Client) (err error) {
	ok := ValidUID(UID, client)
	if ok == false {
		return ErrUserNotFound
	}
	ok = ValidTID(TID, client)
	if ok == false {
		return ErrTeamNotFound
	}
	key, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	if err != nil {
		return err
	}
	fmt.Println("join")
	bin := as.NewBin("CurrentTeam", TID)
	err = client.PutBins(nil, key, bin)
	return err
}

//LeaveTeam set UID current team to 0 will not remove entry from  team TID
func LeaveTeam(UID int, client *as.Client) (err error) {
	ok := ValidUID(UID, client)
	if ok == false {
		return ErrUserNotFound
	}
	key, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	if err != nil {
		return err
	}
	bin := as.NewBin("CurrentTeam", 0)
	err = client.PutBins(nil, key, bin)
	return err
}

//checkUserHaveTeam check if user have a team
func checkUserHaveTeam(UID int, client *as.Client) (ok bool, err error) {
	key, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	if err != nil {
		return false, err
	}
	rec, err := client.Get(nil, key, "CurrentTeam")
	if rec == nil {
		return false, nil
	}
	TID, ok := rec.Bins["CurrentTeam"].(int)
	if ok == false {
		return false, nil
	}
	if TID == 0 {
		return false, nil
	}
	return true, nil
}
func DeleteAccount(ID string, client *as.Client) {
	key, err := as.NewKey(DBNamespace, AccountSet, ID)
	rec, err := client.Get(nil, key, binUID, binID)
	if err != nil {
		fmt.Println(err)
		return
	}
	if rec == nil {
		fmt.Println("Account Not Found")
		return
	}

	UID := rec.Bins[binUID].(int)
	Dkey, err := as.NewKey(DBNamespace, UserSaveSet, UID)
	fmt.Printf("user id : %v , user UID : %v \n", rec.Bins[binID], rec.Bins[binUID])
	fmt.Println(UID)
	ok := ValidUID(UID, client)
	fmt.Printf("ok : %v\n", ok)
	if ok == true {
		fmt.Println("exist")
	}
	existed, err := client.Delete(nil, Dkey)
	fmt.Println("Data:")
	fmt.Println(existed)
	existed, err = client.Delete(nil, key)
	fmt.Println("Account:")
	fmt.Println(existed)
}
