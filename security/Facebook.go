package security

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

var netClient = &http.Client{Timeout: time.Second * 20}

//ErrIDNotMatch occur when Id not match
var ErrIDNotMatch = errors.New("ID not Match")

//ErrSessionExpired Session has expired
var ErrSessionExpired = errors.New("Session has expired")

//VerifyFBToken verify access token is correct
func VerifyFBToken(id string, accessToken string) (err error) {
	url := fmt.Sprintf("https://graph.facebook.com/me?fields=id,email&access_token=%s", accessToken)
	fmt.Println(url)
	respone, err := netClient.Get(url)
	fmt.Println(respone)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer respone.Body.Close()

	bodyBytes, err := ioutil.ReadAll(respone.Body)
	if err != nil {
		return err
	}
	//TODO check string
	bodyString := string(bodyBytes)
	fmt.Println(bodyString)
	// check if have id tag
	if strings.Contains(bodyString, "\"id\":") {
		// check id is same
		if strings.Contains(bodyString, id) {
			return nil
		}
		//id not match
		return ErrIDNotMatch
	}
	// check
	if strings.Contains(bodyString, "Session has expired") {
		return ErrSessionExpired
	}
	if strings.Contains(bodyString, "The session was invalidated previously using an API call") {
		return ErrSessionExpired
	}
	if strings.Contains(bodyString, "<html>") {
		return ErrSessionExpired
	}

	return fmt.Errorf("Unknown err, string from API : %s ", bodyString)
}
