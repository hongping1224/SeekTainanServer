package Server

import (
	"fmt"
)

type errInvalidVar struct {
	VarName  string
	VarType  string
	RecValue interface{}
}

func (e *errInvalidVar) Error() string {
	return fmt.Sprintf("Invalid Var - VarName  : %s , VarType : %s , ReceiveValue : %v", e.VarName, e.VarType, e.RecValue)
}
