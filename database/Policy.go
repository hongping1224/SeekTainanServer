package database

import (
	"time"

	as "github.com/aerospike/aerospike-client-go"
)

//DefatultBasePolicy change aerospike default policy
var DefatultBasePolicy = &as.BasePolicy{
	Priority:            as.DEFAULT,
	ConsistencyLevel:    as.CONSISTENCY_ONE,
	Timeout:             0 * time.Millisecond,
	SocketTimeout:       1000 * time.Millisecond,
	MaxRetries:          2,
	SleepBetweenRetries: 1 * time.Millisecond,
	SleepMultiplier:     1.0,
	ReplicaPolicy:       as.SEQUENCE,
	SendKey:             false,
	LinearizeRead:       false,
}

//DefatultClientPolicy change aerospike default client policy
var DefatultClientPolicy = &as.ClientPolicy{
	Timeout:                     30 * time.Second,
	IdleTimeout:                 14 * time.Second,
	ConnectionQueueSize:         256,
	FailIfNotConnected:          true,
	TendInterval:                time.Second,
	LimitConnectionsToQueueSize: true,
	RequestProleReplicas:        false,
	IgnoreOtherSubnetAliases:    false,
}

//DefaultWritePolicy change aerospike default write policy
var DefaultWritePolicy = &as.WritePolicy{
	BasePolicy:         *DefatultBasePolicy,
	RecordExistsAction: as.UPDATE,
	GenerationPolicy:   as.NONE,
	CommitLevel:        as.COMMIT_ALL,
	Generation:         0,
	Expiration:         0,
}

//DefaultBatchPolicy change aerospike default Batch policy
var DefaultBatchPolicy = &as.BatchPolicy{
	BasePolicy:          *DefatultBasePolicy,
	ConcurrentNodes:     1,
	UseBatchDirect:      false,
	AllowInline:         true,
	AllowPartialResults: false,
	SendSetName:         false,
}

//DefaultScanPolicy change aerospike default Scan policy
var DefaultScanPolicy = &as.ScanPolicy{
	MultiPolicy: DefaultMultiPolicy,
	ScanPercent: 100,

	ConcurrentNodes: true,
	IncludeLDT:      false,
}

//DefaultMultiPolicy change aerospike default Multi policy
var DefaultMultiPolicy = &as.MultiPolicy{
	BasePolicy:                 DefatultBasePolicy,
	MaxConcurrentNodes:         0,
	RecordQueueSize:            50,
	IncludeBinData:             true,
	WaitUntilMigrationsAreOver: false,
}

//DefaultQueryPolicy change aerospike default Query policy
var DefaultQueryPolicy = &as.QueryPolicy{
	MultiPolicy: DefaultMultiPolicy,
}

//DefaultAdminPolicy change aerospike default Admin policy
var DefaultAdminPolicy = &as.AdminPolicy{
	Timeout: 1 * time.Second,
}

var a = as.NewScanPolicy()
