package database

import (
	"encoding/json"
)

//Account stores user login data
type Account struct {
	ID       string
	PassHash string
	UID      int
	//AccType Account type , 1 is email , 2 is facebook ,3 is Gplus
	AccType int
}

const (
	//Email is email user
	Email int = 1
	//Facebook is Facebook user
	Facebook int = 2
	//Gplus is Gplus user
	Gplus int = 3
)

//UserSave is user save data
type UserSave struct {
	UID         int
	Name        string
	PlayerSave  string
	CurrentTeam int
}

//TeamSave is Team save data
type TeamSave struct {
	TID       int
	Progress  []interface{}
	Logs      []interface{}
	LogTeller []interface{}
}

//UnmarshalJSON convert json to EmailUser
func (u *Account) UnmarshalJSON(data []byte) error {
	var v map[string]interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	u.ID = v["Email"].(string)
	u.PassHash = v["Password"].(string)
	u.UID = int(v["UID"].(float64))
	return nil
}

//UnmarshalJSON convert json to UserSave
func (u *UserSave) UnmarshalJSON(data []byte) error {
	var v map[string]interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	u.UID = int(v["UID"].(float64))
	u.Name = v["Name"].(string)
	u.PlayerSave = v["PlayerSave"].(string)
	return nil
}
