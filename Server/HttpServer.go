package Server

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/hongping1224/SeekTainanServer/database"
	"gitlab.com/hongping1224/SeekTainanServer/security"

	as "github.com/aerospike/aerospike-client-go"
)

//HTTPServer instance of server
type HTTPServer struct {
	dBClient   *as.Client
	registerer *Registerer
}

//HostPort is the port for the server
const HostPort int = 80

var instance *HTTPServer

//NewHTTPServer use as constructor
func NewHTTPServer(client *as.Client) *HTTPServer {
	// lazy loading
	if instance == nil {
		fmt.Println("Create new http instance")
		instance = new(HTTPServer)
		instance.dBClient = client
		instance.registerer = new(Registerer)
		instance.registerer.Init(instance.dBClient)
	}
	return instance
}

//Start run server, add every start page into this function
func (server *HTTPServer) Start() {
	if server.dBClient == nil {
		fmt.Println("DBClient is not init!")
		return
	}

	// Add Start Page to here
	http.HandleFunc("/Test", test)
	//login and register
	http.HandleFunc("/RegisterViaEmail", registerViaEmail)
	http.HandleFunc("/EmailLogin", loginemail)
	http.HandleFunc("/FBLogin", loginFB)
	http.HandleFunc("/GPlusLogin", loginGplus)

	//User
	http.HandleFunc("/GetUserSave", getUserSave)
	http.HandleFunc("/SaveUserSave", saveUserSave)
	http.HandleFunc("/ChangeUserName", changeUserName)

	//Team
	http.HandleFunc("/CreateNewTeam", createNewTeam)
	http.HandleFunc("/JoinTeam", joinTeam)
	http.HandleFunc("/KickMember", kickMember)
	http.HandleFunc("/LeaveTeam", leaveTeam)
	http.HandleFunc("/GetTeamData", getTeamSave)
	http.HandleFunc("/GetTeamInfo", getTeamInfo)
	http.HandleFunc("/AddTeamLog", addTeamLog)
	http.HandleFunc("/AddTeamProgress", addTeamProgress)
	http.HandleFunc("/AddFakeTeamProgress", addFakeTeamProgress)
	http.HandleFunc("/PunishTeam", punishTeam)
	http.HandleFunc("/GetPunishTime", getPunishTime)
	http.HandleFunc("/SetTeamName", setTeamName)
	http.HandleFunc("/SetTeamNickName", setTeamNickName)
	http.HandleFunc("/SetJob", setJob)

	//other
	http.HandleFunc("/GetServerTime", getServerTime)
	//End of Start Page

	//Start Server
	fmt.Println("Server Starting...")
	err := http.ListenAndServe(":"+strconv.Itoa(HostPort), nil)
	if err != nil {
		fmt.Println("ListenAndServe failed: ", err)
	}
	defer server.Close()
}

//Close stop server
func (server *HTTPServer) Close() {
	fmt.Println("Server Stop Because of some error...")
}

//GetUserSave From database
func (server *HTTPServer) GetUserSave(UID int) (save database.UserSave, err error) {
	return database.GetUserSave(UID, server.dBClient)
}

//SaveUserSave to database
func (server *HTTPServer) SaveUserSave(UID int, PlayerSave string) (err error) {
	return database.SaveUserSave(UID, PlayerSave, server.dBClient)
}

//ChangeUserName to database
func (server *HTTPServer) ChangeUserName(UID int, PlayerName string) (err error) {
	return database.ChangeUserName(UID, PlayerName, server.dBClient)
}

//AddLog LogID to TID
func (server *HTTPServer) AddLog(TID int, UID int, LogID int) (err error) {
	return database.AppendTeamLog(TID, UID, LogID, server.dBClient)
}

//AddProgressWChecks Progress to TID
func (server *HTTPServer) AddProgressWChecks(TID int, Progress int) (err error) {
	return database.AppendTeamProgress(TID, Progress, server.dBClient, true)
}

//AddProgressWOChecks Progress to TID
func (server *HTTPServer) AddProgressWOChecks(TID int, Progress int) (err error) {
	return database.AppendTeamProgress(TID, Progress, server.dBClient, false)
}

//CreateNewTeam and return TID
func (server *HTTPServer) CreateNewTeam(UID int) (TID int, err error) {
	return database.CreateNewTeam(UID, server.dBClient)
}

//GetTeamData return Team progress and logs
func (server *HTTPServer) GetTeamData(TID int) (save database.TeamSave, err error) {
	return database.GetTeamData(TID, server.dBClient)
}

//JoinTeam UID join into TID Team
func (server *HTTPServer) JoinTeam(UID int, TID int) (err error) {
	err = database.JoinTeam(UID, TID, server.dBClient)
	if err != nil {
		fmt.Println(err)
	}
	return err
}

//KickMember UID join into TID Team
func (server *HTTPServer) KickMember(UID int, TID int) (err error) {
	return database.KickMember(UID, TID, server.dBClient)
}

//LeaveTeam Change UID current team to 0 will not remove Entry in TID
func (server *HTTPServer) LeaveTeam(UID int) (err error) {
	return database.LeaveTeam(UID, server.dBClient)
}

//PunishTeam set TID Team punish time to time.now
func (server *HTTPServer) PunishTeam(TID int) {
	database.ResetPunishTime(TID)
}

//GetTeamPunishTime return TID Team starting punish time
func (server *HTTPServer) GetTeamPunishTime(TID int) time.Time {
	return database.GetPunishTime(TID)
}

//SetTeamName of team TID 's Name
func (server *HTTPServer) SetTeamName(TID int, Name string) (err error) {
	return database.SetTeamName(TID, Name, server.dBClient)
}

//SetTeamNickName set member uid name in team TID
func (server *HTTPServer) SetTeamNickName(TID int, UID int, Name string) (err error) {
	err = database.SetTeamMemberName(TID, UID, Name, server.dBClient)
	if err != nil {
		fmt.Println(err)
	}
	return err
}

//GetTeamInfo get team member and team name
func (server *HTTPServer) GetTeamInfo(TID int) (info map[string]interface{}, err error) {
	return database.GetTeamInfo(TID, server.dBClient)
}

//SetJob set member uid name in team TID
func (server *HTTPServer) SetJob(TID int, UID int, Job int) (err error) {
	return database.SetJob(TID, UID, Job, server.dBClient)
}

//CreateAccount create a account and return the UID
func (server *HTTPServer) CreateAccount(ID string, pass string, accType int) (UID int, err error) {
	//acc, err := server.registerer.ViaEmail(ID, pass, accType)
	if accType == 2 {

		return server.registerer.ViaFB(ID, pass)
	} else if accType == 3 {
		return server.registerer.ViaGplus(pass)
	}
	return 0, err
}

//Login get UID
func (server *HTTPServer) Login(ID string, pass string, accType int) (UID int, err error) {
	acc, err := database.GetAccount(ID, server.dBClient)
	if err == database.ErrAccountNotFound {
		//if is no account
		fmt.Println("Account not found")
		if err == database.ErrAccountNotFound {
			//if is fb or gplus login
			if accType == database.Facebook || accType == database.Gplus {
				//create account
				fmt.Println("try create new account")
				return server.CreateAccount(ID, pass, accType)
			}
		}
		return 0, err
	}
	// check password correct
	if security.CheckPasswordHash(pass, acc.PassHash) == true {
		return acc.UID, nil
	}
	// wrong password
	//fmt.Println("Wrong PassWord")
	//facebook
	if acc.AccType == database.Facebook {
		//check new access token is true
		//fmt.Println("Check new Token")
		err := security.VerifyFBToken(acc.ID, pass)
		// if token cannot be verify
		if err != nil {
			//fmt.Println("Check fail")
			//fmt.Println(err.Error())
			return 0, err
		}
		// token verify
		// change pass word
		//fmt.Println("Check success")
		passhash, err := security.HashPassword(pass)
		//fmt.Println("Changing password")
		err = database.ChangePass(acc.ID, passhash, server.dBClient)
		//fmt.Println("Changing success")
		if err != nil {
			return 0, err
		}
		return acc.UID, nil
	} else if acc.AccType == database.Gplus {
		//check new access token is true
		_, err := security.VerifyIDToken(pass)
		// if token cannot be verify
		if err != nil {
			return 0, err
		}
		// token verify
		// change pass word
		passhash, err := security.HashPassword(pass)
		err = database.ChangePass(acc.ID, passhash, server.dBClient)
		if err != nil {
			return 0, err
		}
		return acc.UID, nil
	}

	return 0, errors.New("Wrong Password")
}
