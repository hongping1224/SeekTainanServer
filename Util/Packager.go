package Util

import (
	"encoding/json"
	"fmt"
)

//PackMsg to Json
func PackMsg(success bool, msg interface{}) (parsJSON []byte, err error) {
	// make msg into an json Object
	msgJSON, err := json.Marshal(msg)
	if err != nil {
		return make([]byte, 0), err
	}
	pars := parcel{Info: success, Message: msgJSON[:]}
	parsJSON, err = json.Marshal(pars)

	if err != nil {
		return make([]byte, 0), err
	}
	//return parcel
	return parsJSON, nil
}

//Pack struct to Json
func Pack(obj interface{}) (parsJSON []byte, err error) {
	// make msg into an json Object
	objJSON, err := json.Marshal(obj)
	if err != nil {
		return make([]byte, 0), err
	}
	return objJSON, nil
}

//Unpack Json to map
func Unpack(JSON []byte, obj interface{}) (err error) {
	// make msg into an json Object
	err = json.Unmarshal(JSON, &obj)
	fmt.Println(obj)
	if err != nil {
		return err
	}
	return nil
}

type parcel struct {
	Info    bool
	Message []byte
}
