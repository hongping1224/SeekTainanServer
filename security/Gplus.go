package security

import (
	oauth2 "google.golang.org/api/oauth2/v1"
)

//VerifyIDToken verify idToken recieve from client and get tokeninfo
func VerifyIDToken(idToken string) (*oauth2.Tokeninfo, error) {
	oauth2Service, err := oauth2.New(netClient)
	tokenInfoCall := oauth2Service.Tokeninfo()
	tokenInfoCall.IdToken(idToken)
	tokenInfo, err := tokenInfoCall.Do()
	if err != nil {
		return nil, err
	}
	return tokenInfo, nil
}
