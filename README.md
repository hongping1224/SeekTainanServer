# Introduction

This is a Server for SeekTainanApp.

Database: Aerospike

API:

Login and register :

* [ok]LoginViaFB
* [Add to Unity]LoginViaEmail
* [Add to Unity]LoginViaGPlus
* RegisterViaEmail(string email,string password)

Team Creation :

* [ok]CreateTeam
* [ok]JoinTeam
* [ok]KickMember
* [ok]LeaveTeam

GetData :

* [ok]GetServerTime
* [ok]GetTeamData
* [ok]GetUserData
* [ok]GetTeamInfo

NotifyServer :

* [ok]AddTeamLog
* [ok]AddTeamProgress(Use to answer question, If punish timer is running, return error code punish timer running)
* [ok]AddFakeTeamProgress(Use to unlock quest or get item, ignore punish timer)
* [ok]PunishTeam
* [ok]SaveUserSave
* [ok]SetUserName
* [ok]SetTeamNickName
* [ok]SetTeamName
* [ok]SetJob

## Setup Dependency

``` bash
  go get golang.org/x/crypto/bcrypt
  go get github.com/aerospike/aerospike-client-go
```

## Aerospike Note

### Layers

Namespace -> can be think as a drive, can have different policies and behavior

Sets -> can be think as a table, can have additional policies on top of namespace policies

Records -> can be think as a row in a table, will have a key, metadata and bins

Bins -> fields of a record. something like colume.

### Keys

Primary Index : This is a key, can only to do one to one.

Secondary Index : This acts like label, can perform one to many operations.

### Clients

puts -> add data

get -> get data
> if key not exist, rec will be nil
>
> if key exist , bin not exist, BinsMap will not contain item

delete -> delete data

### DB Structure

#### Example

---

>* ns
>> * sets
>>> * bins

---

> * Data
> > * Account
> > > * ID(key)
> > > * Pass
> > > * UID
> > > * AccType // 1 is email , 2 is facebook ,3 is Gplus
> > * UserCount
> > > * NumOfUser
> > * UserSaves
> > > * UID(key)
> > > * Name
> > > * Savedata
> > > * CurrentTeam
> > * TeamSaves
> > > * TID(key)
> > > * TeamMembers
> > > * LeaderUID
> > > * TeamName
> > > * LevelID
> > > * Progress ->int[]
> > > * StartTime
> > > * FinishTime
> > > * PunishTime
> > > * LogID-> int[]
> > > * LogTeller-> int[]

---

## Password Handling Method

> <https://gowebexamples.com/password-hashing/>

## GoogleTokenVerify

> <https://stackoverflow.com/questions/36716117/validating-google-sign-in-id-token-in-go>

```go
import (
    "google.golang.org/api/oauth2/v2"
    "net/http"
)

var httpClient = &http.Client{}

func verifyIdToken(idToken string) (*oauth2.Tokeninfo, error) {
    oauth2Service, err := oauth2.New(httpClient)
    tokenInfoCall := oauth2Service.Tokeninfo()
    tokenInfoCall.IdToken(idToken)
    tokenInfo, err := tokenInfoCall.Do()
    if err != nil {
        return nil, err
    }
    return tokenInfo, nil
}
```

## FacebookTokenVerify

> <https://graph.facebook.com/me?fields=id,name,email&access_token=@accesstoken>
