package database

import (
	"fmt"

	as "github.com/aerospike/aerospike-client-go"
	Util "gitlab.com/hongping1224/SeekTainanServer/Util"
)

//DBAddress is the address of database server
const DBAddress = "ec2-18-218-122-40.us-east-2.compute.amazonaws.com"

//DBPort is the port for connection
const DBPort = 3000

//DBNamespace is the namespace of database
const DBNamespace = "Data"

//InitDB will setup the database and add all namespace needed
func InitDB() {
	// init client
	fmt.Println("Start Init DB")
	client, err := as.NewClient(DBAddress, DBPort)
	defer client.Close()

	if err != nil {
		fmt.Println(err)
	}
	key, err := as.NewKey(DBNamespace, "Email", "test")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Print("Check Email DB Exist...")

	exist, err := client.Exists(nil, key)
	if err != nil {
		fmt.Println(err)
	}
	if exist == false {
		fmt.Println("\nEmail DB Not Exist...")

		user, err := Util.Pack(Account{ID: "test",
			UID:      0,
			PassHash: "test"})
		if err != nil {
			panic(err)
		}
		bin := as.NewBin("test", user)
		fmt.Println("Initializing Email DB...")
		err = client.AddBins(nil, key, bin)
		if err != nil {
			panic(err)
		}
	}
	fmt.Println("[OK]")

	fmt.Print("Check UserSaves DB Exist...")
	key, err = as.NewKey(DBNamespace, "UserSaves", "test")
	if err != nil {
		fmt.Println(err)
	}
	exist, err = client.Exists(nil, key)
	if err != nil {
		fmt.Println(err)
	}
	if exist == false {
		fmt.Println("\nUserSaves DB Not Exist...")
		bin := as.NewBin("test", "data")
		fmt.Println("Initializing UserSaves DB...")
		client.AddBins(nil, key, bin)

	}
	fmt.Println("[OK]")

	fmt.Print("Check TeamSaves DB Exist...")

	key, err = as.NewKey(DBNamespace, "TeamSaves", "test")
	if err != nil {
		fmt.Println(err)
	}
	exist, err = client.Exists(nil, key)
	if err != nil {
		fmt.Println(err)
	}
	if exist == false {
		fmt.Println("\nTeamSaves DB Not Exist...")
		bin := as.NewBin("test", "data")
		fmt.Println("Initializing TeamSaves DB...")
		client.AddBins(nil, key, bin)
	}
	fmt.Println("[OK]")

	key, err = as.NewKey(DBNamespace, "UserCount", "NumOfUser")
	if err != nil {
		fmt.Println(err)
	}
	exist, err = client.Exists(nil, key)
	if err != nil {
		fmt.Println(err)
	}
	// if there are no data ,  init a data
	if exist == false {
		fmt.Println("\nNumOfUser DB Not Exist...")
		bin := as.NewBin("Count", 1000000)
		fmt.Println("Initializing NumOfUser DB...")
		client.AddBins(nil, key, bin)
	}

	key, err = as.NewKey(DBNamespace, "UserCount", "NumOfTeam")
	if err != nil {
		fmt.Println(err)
	}
	exist, err = client.Exists(nil, key)
	if err != nil {
		fmt.Println(err)
	}
	// if there are no data ,  init a data
	if exist == false {
		fmt.Println("\nNumOfTeam Not Exist...")
		bin := as.NewBin("Count", 1000000)
		fmt.Println("Initializing NumOfTeam DB...")
		client.AddBins(nil, key, bin)
	}

}

//GetUserNum From DB
func GetUserNum(client *as.Client) (int, error) {
	key, err := as.NewKey(DBNamespace, "UserCount", "NumOfUser")
	if err != nil {
		return 0, err
	}
	rec, err := client.Get(nil, key)
	if err != nil {
		//data not found
		return 0, err
	}
	num := rec.Bins["Count"].(int)
	return num, nil
}

//NewDBClient create a new client which work with longer timeout
func NewDBClient() (*as.Client, error) {
	client, err := as.NewClient(DBAddress, DBPort)
	/*	client.DefaultPolicy = DefatultBasePolicy
		client.DefaultBatchPolicy = DefaultBatchPolicy
		client.DefaultWritePolicy = DefaultWritePolicy
		client.DefaultScanPolicy = DefaultScanPolicy
		client.DefaultQueryPolicy = DefaultQueryPolicy
		client.DefaultAdminPolicy = DefaultAdminPolicy*/
	return client, err
}
